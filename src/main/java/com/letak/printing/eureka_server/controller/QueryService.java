package com.letak.printing.eureka_server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.appinfo.InstanceInfo.InstanceStatus;
import com.netflix.discovery.shared.Application;
import com.netflix.eureka.registry.PeerAwareInstanceRegistry;

@RestController
@RequestMapping("/apps")
public class QueryService {

    @Autowired
    PeerAwareInstanceRegistry registry;

    @RequestMapping("")
    ServiceResponse listApps() {
        ServiceResponse result = new ServiceResponse();
        
        List<Application> apps = registry.getApplications().getRegisteredApplications();
        Object[] appNames = apps.stream().map((app) -> { return app.getName(); }).toArray();

        result.setResult("ok");
        result.setData( appNames );
        
        return result;
    }

    @RequestMapping("/{appname}")
    ServiceResponse listForApp(@PathVariable("appname") String appName) {
        ServiceResponse result = new ServiceResponse();

        Application application = registry.getApplication(appName);

        if (application == null) {
            result.setResult("error");
            result.setData("no instances found for '" + appName + "'");
        } else {
            Object[] instances = application.getInstances().stream().filter((inst) -> { if( inst.getStatus() == InstanceStatus.UP ) return true; return false; } ).toArray();
            result.setResult("ok");
            result.setData(instances);
        }
        return result;
    }
}
